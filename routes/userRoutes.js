const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

// Router for checking if the email exists
router.post("/checkEmail", (req, res) =>{
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for the user registration
router.post("/register", (req, res) =>{
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user login/authentication
router.post("/login", (req, res) =>{
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to set user as admin
router.patch("/:userId/setAsAdmin", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
	userControllers.updateRole(req.params.userId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false);
	}
})

// Route to place an order
router.post("/order", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)

	let orderData = {
		userId: userData.id,
		productId: req.body.productId,
		productName: req.body.productName,
		orderCount: req.body.orderCount
	}

	if(userData.isAdmin){
		res.send(false)
	}
	else{
	userControllers.order(orderData).then(resultFromController => res.send(resultFromController));
	}
})

// Route to Retrieving authenticated user's orders
router.get("/:userId/userOrders", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		res.send(false)
	}
	else{
	userControllers.placedOrders(req.params.userId).then(resultFromController => res.send(resultFromController));
	}
})


// Route to Retrieving orders (Admin only)
router.get("/orderList", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		userControllers.orderList(req.params.userId).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false)
	}
})

// Route for the retrieving the current user's details
router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); //contains the token 
	console.log(userData);

	// Provides the user's ID for the getProfile controller method
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

module.exports = router;