const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Route for creating a product
router.post("/createProducts", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
		if(userData.isAdmin){
			productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
		}
		// If the user is NOT Admin, send a response "You don't have permission on this page!"
		else{
			res.send(false);
		}
})

// Route for retrieving all active product
router.get("/", (req, res) =>{
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all product
router.get("/all", (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
		if(userData.isAdmin){
			productControllers.getAll().then(resultFromController => res.send(resultFromController));
		}
		// If the user is NOT Admin, send a response "You don't have permission on this page!"
		else{
			res.send(false);
		}
})

// Route for retrieving a specific product
router.get("/:productId", (req, res) =>{
	console.log(req.params.productId);

	productControllers.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
	productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false);
	}
})

// Route to Archive a course
router.patch("/:productId/archive", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
	productControllers.archiveProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false);
	}
})

module.exports = router;