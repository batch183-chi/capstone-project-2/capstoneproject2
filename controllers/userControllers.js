const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		mobileNo : reqBody.mobileNo
	})

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		else{
			console.log(user);
			return true;
		}
	})
}

// User login
module.exports.loginUser = (reqBody) =>{
	return User.findOne({email: reqBody.email}).then(result =>{	
		if(result == null){
			return false;
		}	
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
	})
}

// Role Update
module.exports.updateRole = (userId, reqBody) => {
	let updatedRole = {
		isAdmin: reqBody.isAdmin
	}
	return User.findByIdAndUpdate(userId, updatedRole).then((roleUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Placing an order
module.exports.order = async (data) =>{
	let orderPrice = await Product.findById(data.productId).then(product =>{
		return product.price
	})
	console.log(data)

	let isUserUpdated = await User.findById(data.userId).then(user =>{
		user.orders.push({
			name: data.productName,
			productId: data.productId,
			orderCount: data.orderCount,
			amount: (data.orderCount*orderPrice)
		})

		return user.save().then((order, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	console.log(isUserUpdated);

	if(isUserUpdated && orderPrice){
		return true;
	}
	// User enrollment failure
	else{
		return false;
	}
}

// User's placed orders
module.exports.placedOrders = (userId) =>{
	return User.findById(userId).then(result =>{
		return result.orders;
	})
}

// List of Orders
module.exports.orderList = () =>{
	return User.find({isAdmin : false}, {password: 0, isAdmin: 0, createdOn: 0}).then(result =>{
		return result;
	})
}

// Retrieve user details
module.exports.getProfile = (data) =>{
	console.log(data)
	return User.findById(data.userId).then(result =>{
		result.password ="";

		return result;
	})
}

// Check if email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// result is equal to an empty array ([]);
		// if result is greater than 0, user is found/already exists.
		if(result.length > 0){
			return true;
		}
		// No duplicate email found
		else{
			return false;
		}
	});
}
