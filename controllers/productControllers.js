const Product = require("../models/Product");

// Creating Products
module.exports.addProduct = (reqBody) =>{

	let newProduct = new Product({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((product, error) =>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}

// Retrieving ALL Active Product
module.exports.getAllActive = () =>{
	return Product.find({isActive: true}).then(result => result);
}

// Retrieving ALL Product
module.exports.getAll = () =>{
	return Product.find({}).then(result => result);
}

// Retrieving a SPECIFIC Product
module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result => result);
}

// Update a Product
module.exports.updateProduct = (productId, reqBody) => {
	let updatedProduct = {
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Archive a Product
module.exports.archiveProduct = (productId, reqBody) =>{
	let archivedProduct = {
		isActive: reqBody.isActive
	}
	return Product.findByIdAndUpdate(productId, archivedProduct).then((productArchive, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}