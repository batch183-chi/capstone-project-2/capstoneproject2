const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName:{
        type: String,
        required: [true, "First Name is required"]
    },
    lastName:{
        type: String,
        required: [true, "Last Name is required"]
    },
    email:{
        type: String,
        required: [true, "Email is required"]
    },
    password:{
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin:{
        type: Boolean,
        default: false
    },
    mobileNo:{
        type: String,
        required: [true, "Mobile No. is required"]
    },
    createdOn:{
        type: Date,
        default: new Date()
    },
    orders: [
        {
            productId:{
                type: String,
                required: [true, "productId is required"]
            },
            productName:{
                type: String,
            },
            purchasedOn:{
                type: Date,
                default: new Date()
            },
            orderCount:{
                type: Number,
                required: [true, "Order count is required"]
            },
            amount:{
                type: Number
            }
        }
    ]
})

module.exports = mongoose.model("User", userSchema)